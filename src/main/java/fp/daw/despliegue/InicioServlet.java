package fp.daw.despliegue;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
@WebServlet("/inicio")
public class InicioServlet extends HttpServlet {
 private static final long serialVersionUID = 1L;
 private DataSource dataSource;

 @Override
 public void init(ServletConfig config) throws ServletException {
 try {
 InitialContext context = new InitialContext();
 dataSource = (DataSource) context.lookup("java:comp/env/jdbc/pf");
 if (dataSource == null)
 throw new ServletException("DataSource desconocido");
 } catch (NamingException e) {
 Logger.getLogger(RegistroServlet.class.getName()).log(Level.SEVERE, null, e);
 }
 }
 @Override
 protected void doGet(HttpServletRequest request, HttpServletResponse response)
		 throws ServletException, IOException {
	 doPost(request, response);
	 }
	 @Override
	 protected void doPost(HttpServletRequest request, HttpServletResponse response)
	 throws ServletException, IOException {
	 HttpSession session = request.getSession(false);
	 Map<Integer, Integer> carrito;

	 if (session != null) {
	 carrito = (HashMap<Integer, Integer>) session.getAttribute("carrito");
	 if (carrito == null) {
	 carrito = new HashMap<>();
	 session.setAttribute("carrito", carrito);
	 }
	 String paramid = request.getParameter("id");
	 if (paramid != null) {
	 int id = Integer.parseInt(paramid);
	 if (carrito.containsKey(id))
	 carrito.put(id, carrito.get(id) + 1);
	 else
	 carrito.put(id, 1);
	 }
	 }
	 Connection connection = null;
	 try {
	 connection = dataSource.getConnection();
	 } catch (SQLException e) {
	 Logger.getLogger(RegistroServlet.class.getName()).log(Level.SEVERE, null, e);
	 } finally {
	 InicioHTML.enviar(response, connection, session);
	 if (connection != null)
	 try {
	 connection.close();
	 } catch (SQLException e) {
	 }
	 }
	 }
	}