package fp.daw.despliegue;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
@WebServlet("/confirmacion")
public class ConfirmacionServlet extends HttpServlet {
 private static final long serialVersionUID = 1L;
 private DataSource dataSource;
 @Override
 public void init(ServletConfig config) throws ServletException {
 try {
 InitialContext context = new InitialContext();
 dataSource = (DataSource) context.lookup("java:comp/env/jdbc/pf");
 if (dataSource == null)
 throw new ServletException("DataSource desconocido");
 } catch (NamingException e) {
 Logger.getLogger(RegistroServlet.class.getName()).log(Level.SEVERE, null, e);
 }
 }
 @Override
 protected void doPost(HttpServletRequest request, HttpServletResponse response)
 throws ServletException, IOException {
 doGet(request, response);
 }
 @Override
 protected void doGet(HttpServletRequest request, HttpServletResponse response)
		 throws ServletException, IOException {
	 String cc = request.getParameter("cc");
	 if (cc != null) {
	 /* se reciben datos del formulario */
	 enviarRespuesta(response, confirmar(cc));
	 } else {
	 /* no se reciben datos de confirmaci�n: se redirecciona a la p�gina de inicio */
	 response.sendRedirect("inicio");
	 }

	 }
	 private String confirmar(String cc) {
	 /*
	 * Par�metro cc: recibe el c�digo de confirmaci�n
	 *
	 * retorna: null si se completa la confirmaic�n con exito, en caso contrario
	 * retorna un mensaje de error.
	 *
	 * Realiza la confirmaci�n con los datos almacenados en la base de datos.
	 *
	 */

	 }
	 private void enviarRespuesta(HttpServletResponse response, String confirmado) throws IOException {
	 /*
	 * Par�metro confimado: recibe true si la confirmaci�n ha tenido exito,
	 * false en caso contrario
	 *
	 *
	 * Completa este m�todo para enviar el contenido web que corresponda en funci�n
	 * del valor del par�metro confirmado.
	 *
	 */
	 PrintWriter out = null;
	 try {
	 response.setCharacterEncoding("utf-8");
	 out = response.getWriter();
	 /* aqu� la sentencias que generan el contenido web */
	 } finally {
	 if (out != null)
	 out.close();
	 }
	 }

	}