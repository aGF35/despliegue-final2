package fp.daw.despliegue;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
@WebServlet("/postregistro")
public class PostregistroServlet extends HttpServlet {
 private static final long serialVersionUID = 1L;
 @Override
 protected void doGet(HttpServletRequest request, HttpServletResponse response)
 throws ServletException, IOException {
 doPost(request, response);
 }
 @Override
 protected void doPost(HttpServletRequest request, HttpServletResponse response)
 throws ServletException, IOException {
 String id = request.getParameter("id");
 String email = request.getParameter("email");
 String password = request.getParameter("password");
 if (id != null && email != null && password != null) {
 /* se reciben datos del formulario */
 PrintWriter out = null;
 try {
 response.setCharacterEncoding("utf-8");
 out = response.getWriter();
 /* escribe aqu� la sentencias que generan el contenido de la p�gina de postregistro */
 } finally {
 if (out != null)
 out.close();
 }
 } else {
 /* no se reciben datos de registro: se redirecciona a la p�gina de inicio */
 response.sendRedirect("inicio");
 }
 }
}
