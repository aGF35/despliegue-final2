package fp.daw.despliegue;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletResponse;
public class LoginRegistroHTML {
 static public void enviar(HttpServletResponse response, String mensaje,
 String id, boolean registro) throws IOException {
 PrintWriter out = null;
 String action = registro ? "registro" : "login";
 String enviar1 = registro ? "Registrar usuario" : "Iniciar sesi�n";
 String enviar2 = registro ? "Registrando usuario..." : "Iniciando sesi�n...";
 try {
 response.setCharacterEncoding("utf-8");
 out = response.getWriter();
 out.println("<!DOCTYPE html>");
 out.println("<html>");
 out.println("<head>");
 out.println("<meta charset=\"UTF-8\" />");
 out.println("<title>Inicio de sesi�n</title>");
 out.print("<link rel=\"stylesheet\" type=\"text/css\" href=\"css/loginregistro.css\"");
 out.println(" media=\"screen\" />");
 out.println("<script type=\"text/javascript\" src=\"js/loginregistro.js\"></script>");
 out.println("</head>");
 out.println("<body onload=\"load()\">");
 out.println("<div class=\"form\">");
 if (mensaje != null)
 out.printf("<div class=\"error\"><p>%s</p></div>\n", mensaje);
 out.printf("<form action=\"%s\" method=\"post\"", action);
 out.printf(" onsubmit=\"return onSubmit('%s')\">\n", enviar2);
 out.println("<p><label for=\"id\" id=\"idlbl\">Usuario</label></p>");
 out.print("<p><input type=\"text\" name=\"id\" required=\"required\" pattern=\"\\w+\" ");
 if (id != null)
 out.printf("value=\"%s\"", id);
 out.println(" /></p>");
 if (registro) {
 out.println("<p><label for=\"email\">eMail</label></p>");
 out.print("<p><input type=\"email\" name=\"email\" required=\"required\" ");
 out.print("pattern=\"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:");
 out.println("[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\" /></p>");
 }
 out.println("<p><label for=\"password\">Contrase�a</label></p>\n");
 out.print("<p><input type=\"password\" name=\"password\"");
 out.println(" required=\"required\" /></p>\n");
 out.print("<p class=\"bottom\"><input id=\"enviar\" type=\"submit\"");
 out.printf(" value=\"%s\" /></p>\n", enviar1);
 out.println("</form>");
 out.println("</div>");
 out.println("</body>");
 out.println("</html>");
 } finally {
 if (out != null)
 out.close();
 }
 }
}
