package fp.daw.despliegue;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
public class InicioHTML {
static public void enviar(HttpServletResponse response, Connection connection, HttpSession
session)
throws IOException {
PrintWriter out = null;
try {
response.setCharacterEncoding("utf-8");
out = response.getWriter();
/*
* Escribe aqu� el c�digo que genere la p�gina de inicio siguiendo
* las especificaciones siguientes:
*
* - Si el par�metro connection recibe el valor null, se ha producido
* un error al intentar conectar con la base de datos. Muestra la
* p�gina, pero en lugar del cat�logo de productos muestra un error.
* En caso contrario utiliza el par�metro connection para leer los
* productos de la base de datos.
*
* - Si el par�metro session es distinto de null, cada producto ir�
* acompa�ado de un enlace "a�adir al carrito" que a�ada una unidad
* del producto al carrito. Para generar la url del enlace:
* request.getRequestURL() + "?id=" + id_de_producto"
*
* - El contendido del men� prinpipal var�a en funci�n de si el par�metro
* session es null o no es null.
*/
} finally {
if (out != null)
out.close();
}
}
}
